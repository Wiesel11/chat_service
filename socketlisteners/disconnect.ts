import { Socket } from "socket.io";

export function Disconnect(socket: Socket) {

    socket.on('disconnect', (reason) => {
        console.log('Socket Disconnected : ', reason);

        /**
         * Prevent Memory Leak
         */
        (socket.handshake.query.uid) ? socket.leave(socket.handshake.query.uid as string) : undefined;
        (socket.handshake.query.nsp) ? socket.leave(socket.handshake.query.nsp as string) : undefined;
    })
}



