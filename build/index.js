"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var database_1 = require("./configs/database");
var logger_1 = require("./configs/logger");
var database_2 = require("./databases/database");
var conversation_1 = require("./models/conversation");
var message_1 = require("./models/message");
var http_1 = require("./server/http");
var logger_2 = require("./server/logger");
var socket_1 = require("./server/socket");
var Application = /** @class */ (function () {
    function Application() {
    }
    Application.prototype.INIT = function (conf) {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        process.on('unhandledRejection', function (ex) {
                            // console.log("Unhandled Execption", ex);
                            logger_2.Logger.Log('Unhandled Exception !!!!!', 'critical');
                            logger_2.Logger.Log(ex, "critical");
                        });
                        process.on('uncaughtException', function (ex) {
                            // console.log("Unhandled Execption", ex);
                            logger_2.Logger.Log('Uncaught Exception !!!!!', 'critical');
                            logger_2.Logger.Log(ex, "critical");
                        });
                        if (conf.GracefullShutdown) {
                            //Gracefull Reload on Non-Windows Environment
                            //Check The documentation on following link
                            //https://nodejs.org/api/process.html
                            /**
                             * @Note :
                             * 'SIGTERM' and 'SIGINT' have default handlers on non-Windows platforms that reset the terminal mode before exiting with code 128 + signal number.
                             * If one of these signals has a listener installed, its default behavior will be removed (Node.js will no longer exit).
                             */
                            process.on('SIGINT', function (code) { return __awaiter(_this, void 0, void 0, function () {
                                var result, error_2;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            _a.trys.push([0, 5, , 6]);
                                            return [4 /*yield*/, http_1.HTTPServer.StopServer()];
                                        case 1:
                                            result = _a.sent();
                                            return [4 /*yield*/, socket_1.SocketServer.CloseServer()];
                                        case 2:
                                            _a.sent();
                                            if (!(result.status == 'closed')) return [3 /*break*/, 4];
                                            //Kill All Database Connections or any other pending Finalizers Like Disconnecting the queue and all
                                            return [4 /*yield*/, database_2.DefaultDatabase.Disconnect()];
                                        case 3:
                                            //Kill All Database Connections or any other pending Finalizers Like Disconnecting the queue and all
                                            _a.sent();
                                            _a.label = 4;
                                        case 4:
                                            //Kill The Process so that It will be restarted by PM2 or any other process manager
                                            process.exit(1);
                                            return [3 /*break*/, 6];
                                        case 5:
                                            error_2 = _a.sent();
                                            logger_2.Logger.Log(error_2, 'critical');
                                            process.exit(1);
                                            return [3 /*break*/, 6];
                                        case 6: return [2 /*return*/];
                                    }
                                });
                            }); });
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        return [4 /*yield*/, logger_2.Logger.CreateLogger(logger_1.LoggerConf.colors)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, database_2.DefaultDatabase.Connect(database_1.DBConfig.dbconf.default)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, conversation_1.Conversation.INIT()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, message_1.Messages.INIT()];
                    case 5:
                        _a.sent();
                        this.httpServer = http_1.HTTPServer.INIT(conf);
                        Object.seal(this.httpServer);
                        logger_2.Logger.Console('Server Started : ', 'info');
                        return [3 /*break*/, 7];
                    case 6:
                        error_1 = _a.sent();
                        logger_2.Logger.Console(error_1, 'error');
                        logger_2.Logger.Console('error in Initialising Application');
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    return Application;
}());
var application = new Application();
application.INIT({ PORT: 8000, AllowCors: false, GracefullShutdown: true });
//# sourceMappingURL=index.js.map