"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Disconnect = void 0;
function Disconnect(socket) {
    socket.on('disconnect', function (reason) {
        console.log('Socket Disconnected : ', reason);
        /**
         * Prevent Memory Leak
         */
        (socket.handshake.query.uid) ? socket.leave(socket.handshake.query.uid) : undefined;
        (socket.handshake.query.nsp) ? socket.leave(socket.handshake.query.nsp) : undefined;
    });
}
exports.Disconnect = Disconnect;
//# sourceMappingURL=disconnect.js.map