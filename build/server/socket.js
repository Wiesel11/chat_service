"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketServer = void 0;
var socket_io_1 = require("socket.io");
var disconnect_1 = require("../socketlisteners/disconnect");
var SocketServer = /** @class */ (function () {
    function SocketServer() {
    }
    SocketServer.INIT = function (http) {
        this.server = new socket_io_1.Server(http, {});
        this.BindEvents();
    };
    SocketServer.BindEvents = function () {
        var _this = this;
        this.server.on("connection", function (socket) {
            console.log('Socket Connected');
            if (!socket.handshake.query.uid) {
                socket.disconnect();
                return;
            }
            /**
             * @Note
             * Send Unique Identifier from client for room it could be anythig like UserID, UserEmail etc
             */
            socket.join(socket.handshake.query.uid);
            /**
             * @Note
             * Send NSP from Client for grouped Message
             */
            (socket.handshake.query.nsp) ? socket.join(socket.handshake.query.nsp) : undefined;
            _this.BindSocketEvents(socket);
        });
    };
    SocketServer.BindSocketEvents = function (socket) {
        (0, disconnect_1.Disconnect)(socket);
    };
    SocketServer.SendEvent = function (msg, roomName, data) {
        this.server.to(roomName).emit('message', { msg: msg, data: data });
    };
    SocketServer.SendCustomEvent = function (eventName, roomName, data) {
        this.server.to(roomName).emit(eventName, { data: data });
    };
    SocketServer.CloseServer = function () {
        this.server.close();
    };
    return SocketServer;
}());
exports.SocketServer = SocketServer;
//# sourceMappingURL=socket.js.map