"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = void 0;
var database_1 = require("../databases/database");
var UserModel = /** @class */ (function () {
    function UserModel() {
    }
    UserModel.INIT = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                database_1.DefaultDatabase.db.subscribe(function (val) { return __awaiter(_this, void 0, void 0, function () {
                    var _a, error_1, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                this.db = val;
                                if (!val) return [3 /*break*/, 7];
                                _c.label = 1;
                            case 1:
                                _c.trys.push([1, 3, , 7]);
                                _a = this;
                                return [4 /*yield*/, this.db.createCollection('users')];
                            case 2:
                                _a.collection = _c.sent();
                                console.log('GOT DB');
                                console.log(this.collection.collectionName);
                                return [3 /*break*/, 7];
                            case 3:
                                error_1 = _c.sent();
                                if (!(error_1.code == 48)) return [3 /*break*/, 5];
                                _b = this;
                                return [4 /*yield*/, this.db.collection('users')];
                            case 4:
                                _b.collection = _c.sent();
                                return [3 /*break*/, 6];
                            case 5:
                                console.log(error_1);
                                console.log('error in Creating Collection');
                                _c.label = 6;
                            case 6: return [3 /*break*/, 7];
                            case 7: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    UserModel.InsertUser = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var doc, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.collection.insertOne(data)];
                    case 1:
                        doc = _a.sent();
                        // if (doc && doc.insertedCount) return doc.result;
                        // else return doc;
                        return [2 /*return*/, doc];
                    case 2:
                        error_2 = _a.sent();
                        console.log(error_2);
                        console.log('Error in inserting');
                        return [2 /*return*/, error_2];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserModel.Exists = function (email) {
        return __awaiter(this, void 0, void 0, function () {
            var doc, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.collection.find({ email: email }).limit(1).toArray()];
                    case 1:
                        doc = _a.sent();
                        console.log(!!(doc && doc.length));
                        return [2 /*return*/, !!(doc && doc.length)];
                    case 2:
                        error_3 = _a.sent();
                        console.log(error_3);
                        console.log('Error in inserting');
                        throw error_3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserModel.Login = function (email, password) {
        return __awaiter(this, void 0, void 0, function () {
            var doc, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.collection.find({ email: email, password: password }, {
                                projection: {
                                    password: 0
                                }
                            }).limit(1).toArray()];
                    case 1:
                        doc = _a.sent();
                        return [2 /*return*/, (doc && doc.length) ? doc[0] : undefined];
                    case 2:
                        error_4 = _a.sent();
                        console.log(error_4);
                        console.log('Error in inserting');
                        throw error_4;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return UserModel;
}());
exports.UserModel = UserModel;
//# sourceMappingURL=usersmodel.js.map