import mongodb, { Db, ObjectId } from "mongodb";
import { DefaultDatabase } from "../databases/database";

export interface Message {
    cid: string | ObjectId,
    from: string,
    to: string,
    body: string,
    attachments?: Array<any>
    createdAt: string,
    state?: "SEEN" | "Delivered" | 'Sent' | 'Failed'
}

export abstract class Messages {

    static db: Db;
    static collection: mongodb.Collection;

    public static async INIT() {
        DefaultDatabase.db.subscribe(async val => {
            this.db = val;
            if (val) {
                try {
                    this.collection = await this.db.createCollection('messages');
                    console.log('GOT DB');
                    console.log(this.collection.collectionName);

                } catch (error: any) {
                    if (error.code == 48) {
                        this.collection = await this.db.collection('messages');
                    } else {

                        console.log(error);
                        console.log('error in Creating Collection');
                    }
                }
            }
        });
    }


    public static async InsertMessage(msg: Message) {
        try {

            let doc = await this.collection.insertOne(msg);
            // if (doc && doc.insertedCount) return doc.result;
            // else return doc;
            return doc;
        } catch (error) {
            console.log(error);
            console.log('Error in inserting Conversation');
            return error;
        }
    }



    public static async GetMessages(cid: string) {

        try {

            let doc = await this.collection.find({ cid: cid }).toArray();
            return doc;
        } catch (error) {
            console.log(error);
            console.log('Error in inserting');
            throw error;
        }
    }

    public static async UpdateMessageStatus(id: string, state: "SEEN" | "Delivered" | 'Sent' | 'Failed') {

        try {

            let temp = new Object(id);

            let doc = await this.collection.findOneAndUpdate({ _id: temp }, { $set: { state: state } }, { returnDocument: 'after' });
            return (doc && doc.value) ? doc.value : undefined;
        } catch (error) {
            console.log(error);
            console.log('Error in inserting');
            throw error;
        }
    }
}