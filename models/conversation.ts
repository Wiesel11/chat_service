import mongodb, { Db, ObjectId } from "mongodb";
import { DefaultDatabase } from "../databases/database";

export interface Conversation {
    user1: string,
    user2: string,
    createdDate: string,
    createdBy: string,
    lastUpdatedAt: string,
    _id?: ObjectId | string,
    state?: string
    [key: string]: any
}

export abstract class Conversation {

    static db: Db;
    static collection: mongodb.Collection;

    public static async INIT() {
        DefaultDatabase.db.subscribe(async val => {
            this.db = val;
            if (val) {
                try {
                    this.collection = await this.db.createCollection('conversation');
                    console.log('GOT DB');
                    console.log(this.collection.collectionName);

                } catch (error: any) {
                    if (error.code == 48) {
                        this.collection = await this.db.collection('conversation');
                    } else {

                        console.log(error);
                        console.log('error in Creating Collection');
                    }
                }
            }
        });
    }


    public static async InsertConversation(data: Conversation) {
        try {

            let doc = await this.collection.insertOne(data);
            // if (doc && doc.insertedCount) return doc.result;
            // else return doc;
            return doc;
        } catch (error) {
            console.log(error);
            console.log('Error in inserting Conversation');
            return error;
        }
    }



    public static async GetConversationByID(id: string) {

        try {

            let doc = await this.collection.find({ email: id }).limit(1).toArray();


            return (doc && doc.length) ? doc[0] : undefined;
        } catch (error) {
            console.log(error);
            console.log('Error in Getting Conversation By Id');
            throw error;
        }
    }


    public static async GetConversationByUserID(userID: string) {

        try {

            let doc = await this.collection.find({
                $or: [
                    { user1: userID },
                    { user2: userID }
                ]
            }).limit(1).toArray();


            return (doc && doc.length) ? doc[0] : undefined;
        } catch (error) {
            console.log(error);
            console.log('Error in Getting Conversation By UserID');
            throw error;
        }
    }


    public static async DeleteConversation(id: string) {

        try {

            let doc = await this.collection.findOneAndUpdate({ email: id },
                { $set: { state: 'deleted' } },
                { returnDocument: 'after' });
            return (doc) ? doc.value : undefined;

        } catch (error) {
            console.log(error);
            console.log('Error in Deleting Conversation');
            throw error;
        }
    }
}