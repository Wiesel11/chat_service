import { Server, Socket } from "socket.io";
import http from 'http';
import { Disconnect } from "../socketlisteners/disconnect";

export class SocketServer {



    public static server: Server;

    public static INIT(http: http.Server) {
        this.server = new Server(http, {})
        this.BindEvents()
    }

    private static BindEvents() {
        this.server.on("connection", (socket: Socket) => {
            console.log('Socket Connected');
            if (!socket.handshake.query.uid) {
                socket.disconnect();
                return;
            }
            /**
             * @Note
             * Send Unique Identifier from client for room it could be anythig like UserID, UserEmail etc
             */
            socket.join(socket.handshake.query.uid as string);


            /**
             * @Note
             * Send NSP from Client for grouped Message 
             */
            (socket.handshake.query.nsp) ? socket.join(socket.handshake.query.nsp as string) : undefined;

            this.BindSocketEvents(socket);

        });
    }

    public static BindSocketEvents(socket: Socket) {

        Disconnect(socket);

    }

    public static SendEvent(msg: string, roomName: string, data: any) {
        this.server.to(roomName).emit('message', { msg: msg, data: data });
    }

    public static SendCustomEvent(eventName: string, roomName: string, data: any) {
        this.server.to(roomName).emit(eventName, { data: data });
    }

    public static CloseServer() {
        this.server.close();
    }
}